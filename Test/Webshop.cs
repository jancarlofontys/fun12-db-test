﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// For SQL server operations
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Test
{
    class Webshop
    {
        private static string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + Application.StartupPath + @"\Database1.mdf;Integrated Security=True";
        private SqlConnection conn = new SqlConnection(connectionString);

        private List<Product> products = new List<Product>();

        public List<Product> GetAllProducts()
        {
            products = new List<Product>();

            string query = "SELECT * FROM Product";
            conn.Open();
            SqlCommand cmd = new SqlCommand(query, conn);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    Product product = new Product();
                    product.SetName(reader.GetString(1));
                    product.SetPrice(reader.GetInt32(2));

                    products.Add(product);
                }
            }
            conn.Close();
            return products;
        }

        //public void GetProducts()
        //{
        //    conn.Open();

        //    string query = "SELECT * FROM Product";
        //    SqlCommand cmd = new SqlCommand(query, conn);

        //    using (SqlDataReader reader = cmd.ExecuteReader())
        //    {
        //        while (reader.Read())
        //        {
        //            Product product = new Product();

        //            product.SetName(reader.GetString(0));
        //            product.SetPrice(reader.GetInt32(1));

        //            //int id = reader.GetInt32(0);
        //            //string naam = reader.GetString(1);
        //            //int price = reader.GetInt32(2);

        //            //Console.WriteLine(id + " " + naam + " " + price);
        //        }
        //    }

        //    conn.Close();
        //}
    }
}
