﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Webshop w = new Webshop();
            List<Product> products = w.GetAllProducts();

            //lblName.Text = Convert.ToString(products[0].GetName());
            //lblPrice.Text = Convert.ToString(products[0].GetPrice());

            foreach(Product product in products)
            {
                string str = product.GetName() + " " + product.GetPrice();
                lbProducts.Items.Insert(0, str);

                //Console.WriteLine(str);
            }
        }
    }
}
