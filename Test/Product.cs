﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }

        public int GetID()
        {
            return ID;
        }
        public void SetName(string name)
        {
            Name = name;
        }
        public string GetName()
        {
            return Name;
        }
        public void SetPrice(int price)
        {
            Price = price;
        }
        public int GetPrice()
        {
            return Price;
        }

        public override string ToString()
        {
            return "[" + ID + "]" + Name + " " + Price;
        }
    }
}
